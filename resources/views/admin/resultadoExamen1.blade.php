@extends('layouts.appAdmin')
@section('content')
    <br>
    <div class="card">
        <div class="card-body">
            <h1>Resultados de Examen {{$examen->titulo}}</h1>
            <h3>Preguntas correctas: {{$preguntasCorrectas}}</h3>
            <h3>Preguntas Incorrectas: {{$preguntasIncorrectas}}</h3>
            <h3>Calificacion: {{$cali}}</h3>
            <br>
            <a href="{{route('admin.index')}}" class="btn-success btn">Regresar</a>
        </div>
    </div>
@endsection
