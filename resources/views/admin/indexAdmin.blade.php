@extends('layouts.appAdmin')

@section('cs')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css">
@endsection

@section('content')
    <h1>Examenes</h1>
    <div class="row justify-content-end">
        <div class="col-4">
            <a href="{{route('crear.examen')}}" class="btn btn-primary">Crear Examenes</a>
        </div>
    </div>
    <div class="row justify-content-start">
        <div class="col-4">
            <i>Si no hay examenes disponibles cree uno.</i>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-striped" id="tablaExam">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>No.Preguntas</th>
                    <th>Realizar</th>
                    <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($exams as $exam)
                    <tr>
                        <td>{{$exam->id}}</td>
                        <td>{{$exam->titulo}}</td>
                        <td>{{$exam->numeroP}}</td>
                        <td>
                            <a href="{{route('realizar.examen',$exam->id)}}" class="btn btn-success"><i class="fas fa-play"></i></a>
                        </td>
                        <td>
                           <form action="{{route('eliminar.examen',$exam->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script>
    <script>
        $('#tablaExam').DataTable({
            responsive:true,
            autoWidth: false
        });
    </script>
@endsection
