@extends('layouts.appAdmin')
@section('content')
    <br>
    <div class="card">
        <div class="card-body">
            <h1>Examen {{$examen->titulo}}</h1>
            <form action="{{route('resultados.examen',$idE=$examen->id)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    {{$num=1}}
                    @foreach($preguntas as $pregunta)
                            <label for="exampleFormControlInput1">{{$pregunta->pregunta}}</label>
                            @foreach($respuestas as $respuesta)
                                @if($respuesta->id_pregunta==$pregunta->id)
                                <br>
                                <label for="exampleFormControlInput1"> -.{{$respuesta->respuesta}}</label>
                                @endif
                            @endforeach
                            <br>
                            <label for="">Elige la respuesta correcta</label>
                            <input id="prodId" name="idPregunta{{$num}}" type="hidden" value="{{$pregunta->id}}">
                            <select class="form-control" name="respuesta{{$num}}" required>
                            @foreach($respuestas as $respuesta)
                                @if($respuesta->id_pregunta==$pregunta->id)
                                        <option value="{{$respuesta->respuesta}}">{{$respuesta->respuesta}}</option>
                                @endif
                            @endforeach
                            </select>
                            <br>
                        <p style="visibility: hidden">{{$num++}}</p>
                    @endforeach
                </div>
                <input type="submit" class="btn btn-success" value="Enviar Examen">
            </form>
        </div>
    </div>
@endsection
