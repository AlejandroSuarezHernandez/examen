@extends('layouts.appAdmin')
@section('content')
    <br>
<div class="card">
    <div class="card-body">
        <h1>Crear Examen</h1>
        <form action="{{route('crear.examen2')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="exampleFormControlInput1">Nombre del Examen</label>
                @if(isset($estatus))
                    @if($estatus=="success")
                        <label class="text-success">{{$mensaje}}</label>
                    @elseif($estatus=="error")
                        <label class="text-warning">{{$mensaje}}</label>
                    @endif
                @endif
                <input type="text" class="form-control" id="exampleFormControlInput1" name="nombre" placeholder="Ingrese el nombre del examen" required>
                <br>
                <label for="exampleFormControlInput2">Numero de preguntas</label>
                <input type="number" class="form-control" id="exampleFormControlInput2" name="preguntas" placeholder="Ingrese el numero de preguntas "min="2" max="50" required>
                <br>
                <label for="inputState">Numero de respuestas</label>
                <input type="number" class="form-control" id="inputState" name="respuestas" placeholder="Ingrese el numero de respuestas" min="2" max="4" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Crear Examen">
        </form>
    </div>
</div>
@endsection
