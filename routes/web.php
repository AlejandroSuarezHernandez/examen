<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('welcome');
});
Route::get('/welcome',[\App\Http\Controllers\UsuarioController::class,'welcome'])->name('welcome');
Route::get('/login',[\App\Http\Controllers\UsuarioController::class,'login'])->name('login');
Route::post('/login', [\App\Http\Controllers\UsuarioController::class,'VerificarCredenciales'])->name('login.form');
Route::get('/cerrarSesion',[\App\Http\Controllers\UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');

Route::prefix('/usuario')->middleware("VerificarUsuario")->group(function (){
    Route::get('/index',[\App\Http\Controllers\UsuarioController::class,'index'])->name('user.index');
});
Route::prefix('/admin')->middleware("VerificarAdmin")->group(function (){
    Route::get('/index',[\App\Http\Controllers\AdminController::class,'index'])->name('admin.index');
    Route::delete('/examen/{id}',[\App\Http\Controllers\AdminController::class,'eliminarExamen'])->name('eliminar.examen');
    Route::get('/crear',[\App\Http\Controllers\AdminController::class,'crearExamen'])->name('crear.examen');
    Route::post('/crear2',[\App\Http\Controllers\AdminController::class,'crearExamen2'])->name('crear.examen2');
    Route::get('/crear3/{nombre}',[\App\Http\Controllers\AdminController::class,'crearExamen3'])->name('crear.examen.3');
    Route::post('/crear4/{id}',[\App\Http\Controllers\AdminController::class,'crearExamen4'])->name('crear.examen.4');
    Route::post('/resultadosExamen/{idE}',[\App\Http\Controllers\AdminController::class,'resultadosExamen'])->name('resultados.examen');
    Route::get('/examen/{id}',[\App\Http\Controllers\AdminController::class,'realizarExamen'])->name('realizar.examen');
    Route::get('/examen/salir',[\App\Http\Controllers\AdminController::class,'cerrarSesion'])->name('cerrar.sesion');

});
