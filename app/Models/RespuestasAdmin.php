<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestasAdmin extends Model
{
    protected $table = "respuestasadmin";
}
