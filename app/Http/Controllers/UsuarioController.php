<?php

namespace App\Http\Controllers;

use App\Models\Administrador;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\If_;

class UsuarioController extends Controller
{
    public function welcome(){
        return view('welcome');
    }
    public function login(){
        return view('auth.login');
    }
    public function VerificarCredenciales(Request $datos){
        if (!$datos->correo || !$datos->contra){
            return view("auth.login",["estatus"=>"error","mensaje"=>"!Completa los campos!"]);
        }
        $admin = Administrador::where("correo",$datos->correo)->first();
        $usuario = Usuario::where("correo",$datos->correo)->first();
        if (!$admin&&!$usuario)
            return view("auth.login",["estatus"=>"error","mensaje"=>"!El correo no esta registrado!"]);
        else{
         if ($admin){
             if ($datos->contra!=$admin->password)
             //if(!Hash::check($datos->contra,$admin->password))
                 return view("auth.login",["estatus"=>"error","mensaje"=>"!Datos Incorrectos!"]);
             else
                 Session::put('admin',$admin);
             if(isset($datos->url)){
                 $url = decrypt($datos->url);
                 return redirect($url);
             }else{
                 return redirect()->route('admin.index');
             }


         }
         elseif($usuario){
             if ($datos->contra!=$usuario->password)
             //if(!Hash::check($datos->contra,$usuario->password))
                 return view("auth.login",["estatus"=>"error","mensaje"=>"!Datos Incorrectos!"]);
             else
                 Session::put('usuario',$usuario);
                var_dump(Session::get('usuario'));
             if(isset($datos->url)){
                 $url = decrypt($datos->url);
                 return redirect($url);
             }else{
                 return redirect()->route('user.index');
             }
         }
        }

    }
    public function cerrarSesion(){
        if(Session::has('usuario')){
            Session::forget('usuario');
        }
        elseif (Session::has('admin')){
            Session::forget('admin');
        }


        return redirect()->route('login.form');
    }
    public function index(){
        return view('user.indexUser');
    }

}
