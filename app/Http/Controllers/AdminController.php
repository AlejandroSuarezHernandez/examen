<?php

namespace App\Http\Controllers;

use App\Models\Examen;
use App\Models\Preguntas;
use App\Models\Respuestas;
use App\Models\RespuestasAdmin;
use App\Models\resultados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MongoDB\Driver\Session;

class AdminController extends Controller
{
    public  function index(){
        $exams = Examen::all();
        $resultado=Resultados::all();
        $r=0;
        return view('admin.indexAdmin',compact('exams','r'));
    }

    public function eliminarExamen($id){

        $eliminarE=Examen::find($id);
        $verificar=$eliminarE->delete();

        return redirect(route('admin.index'));
    }
    public function crearExamen(){
        return view('admin.crearExamen1');
    }

    public function crearExamen2(Request $datos){
        if(!$datos->nombre || !$datos->preguntas || !$datos->respuestas)
            return view('admin.crearExamen1',["estatus"=> "error", "mensaje"=> "¡Falta información no se puedo crear el Examen!"]);

        $examen = Examen::where('titulo',$datos->nombre)->first();
        if($examen)
            return view('admin.crearExamen1',["estatus"=> "error", "mensaje"=> "¡El nombre ya se encuentra registrado!"]);

        $nombre = $datos->nombre;
        $preguntas = $datos->preguntas;
        $respuestas = $datos->respuestas;

        $examen = new Examen();
        $examen->titulo =  $nombre;
        $examen->numeroP = $preguntas;
        $examen->numeroR = $respuestas;
        $examen->save();
        return redirect(route('crear.examen.3',$nombre));
    }
    public function crearExamen3($nombre){
        $examen = Examen::where('titulo',$nombre)->first();
        return view('admin.crearExamen2',compact('examen'));
    }
    public function crearExamen4(Request $datos,$id){
        $examen = Examen::where('id',$id)->first();

        for ($i=1;$examen->numeroP>=$i;$i++){
            $idExamen=$examen->id;
            $preguntaN='pregunta'.$i;
            $respuestaN='respuestaC'.$i;
            $pregunta=$datos->$preguntaN;
            $respuestaC=$datos->$respuestaN;
            //echo json_encode($datos->$preguntaN) ;
            $preguntas = new Preguntas();
            $preguntas -> id_examen=$idExamen;
            $preguntas->pregunta=$pregunta;
            $preguntas->respuestaC=$respuestaC;
            $preguntas->save();
            for ($j=1;$examen->numeroR>=$j;$j++){
                $idPregunta=Preguntas::where('pregunta',$pregunta)->first();
                $idPregunta2=$idPregunta->id;
                $respuestasN='respuesta'.$j.$i;
                $respuesta=$datos->$respuestasN;
                $respuestaModel= new Respuestas();
                $respuestaModel->id_pregunta=$idPregunta2;
                $respuestaModel->id_examen=$idExamen;
                $respuestaModel->respuesta=$respuesta;
                $respuestaModel->save();
            }

        }
        return redirect(route('admin.index'));
    }
    public function  realizarExamen($id){
        $examenId=$id;
        $examen=Examen::where('id',$id)->first();
        $preguntas = DB::table('preguntas')->where('id_examen', $examenId)->get();
        $respuestas = DB::table('respuestas')->where('id_examen', $examenId)->get();
        return view('admin.relizarExamen1',compact('examen','preguntas','respuestas'));
    }
    public function resultadosExamen(Request $datos,$idE){
        $examen = Examen::where('id',$idE)->first();
        $idAdmin=session('admin')->id;;
        for ($i=1;$examen->numeroP>=$i;$i++){
            $idExamen=$examen->id;
            $idPreguntaN='idPregunta'.$i;
            $respuestaN='respuesta'.$i;
            $pregunta=$datos->$idPreguntaN;
            $respuestaC=$datos->$respuestaN;
            $respuestasA = new RespuestasAdmin();
            $respuestasA-> id_admind=$idAdmin;
            $respuestasA->id_examen=$idE;
            $respuestasA->id_pregunta=$pregunta;
            $respuestasA->respuesta=$respuestaC;
            $respuestasA->save();

        }
        $resultados = DB::table('respuestasa')->where('id_admind',$idAdmin AND 'id_examen',$idE)->get();
        $preguntasComp = DB::table('preguntas')->where('id_examen', $examen->id)->get();
        $preguntasCorrectas=0;
        $preguntasIncorrectas=0;

        foreach ($preguntasComp as $p){
            foreach ($resultados as $r){
                if ($r->id_pregunta==$p->id){
                    if ($r->respuesta==$p->respuestaC){
                    $preguntasCorrectas++;
                    }
                    else{
                        $preguntasIncorrectas++;
                    }
                }

            }
        }
        $pT=$preguntasCorrectas+$preguntasIncorrectas;
        $cali=(10/$pT)*$preguntasCorrectas;
        $result= new resultados();
        $result->id_admin=$idAdmin;
        $result->id_examen=$idE;
        $result->preguntasCorrectas=$preguntasCorrectas;
        $result->preguntasIncorrectas=$preguntasIncorrectas;
        $result->calificacion=$cali;
        $result->save();

        $resultadoFinal = Resultados::all();
        return view('admin.resultadoExamen1',compact('preguntasCorrectas','preguntasIncorrectas','cali','examen'));
    }
    public function cerrarSesion(){
        return view('auth.login');
    }
}
